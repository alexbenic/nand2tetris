// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// --------------
// DECLARATION
// --------------

  // r0 = RAM[0]
  @0
  D=M
  @r0
  M=D

  // r1 = RAM[1]
  @1
  D=M
  @r1
  M=D

  // set RAM[2] to 0
  @0
  D=A
  @2
  M=D

  // i = 0 -- counter
  @0
  D=A
  @i
  M=D

  // mul = 0 -- result
  @0
  D=A
  @mul
  M=D

// ---------------

// --------------
// LOGIC
// --------------

(FOR)
  @i
  D=M
  @r1
  D=D-M
  @LOAD       // if i == r0 load result and terminate
  D;JEQ
  @ADD        // else add
  0;JMP

(ADD)
  @r0
  D=M
  @mul
  D=D+M
  M=D

  @i          // inc i
  M=M+1

  @FOR        // loop
  0;JMP

// --------------

// --------------
// TERMINATE
// --------------

(LOAD)
  @mul
  D=M
  @2
  M=D

(END)
  @END
  0;JMP

  // --------------
