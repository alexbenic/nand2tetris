// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel. When no key is pressed, the
// program clears the screen, i.e. writes "white" in every pixel.

// --------------
// DECLARATION
// --------------

  // bool : color -- -1 is black, 0 is white
  @0
  D=A
  @color
  M=D

  // const : whole screen -- 32 columns of words * 256 rows ( 32 * 256 = 8192 )
  @8192
  D=A
  @monitor
  M=D

  // var : counter = 0
  @0
  D=A
  @i
  M=D

  // var : offset = 0
  @0
  D=A
  @offset
  M=D

// ---------------

// --------------
// READ INPUT
// --------------

(INPUT)
  @KBD
  D=M
  @BLACK          // if KBD > 0; call BLACK
  D;JNE

  @WHITE          // else call WHITE
  0;JMP

// ---------------

// --------------
// BLACK
// --------------
// Check if SCREEN @color is black (1)
// true  -> return to INPUT
// fales -> CHANGE COLOR

(BLACK)
  @color          // check if screen is already black; @color =/= 0
  D=M
  @INPUT
  D;JNE           // if true, jump to INPUT routine

  @CCOLOR         // else set color to -1 and jump to PAINT
  0;JMP

// ---------------

// --------------
// WHITE
// --------------
// Check if SCREEN @color is white (0)
// true  -> return to INPUT
// fales -> CHANGE COLOR

(WHITE)
  @color          // check if screen is already white
  D=M
  @INPUT          // if true, jump to INPUT routine
  D;JEQ

  @CCOLOR         // else set color to 0 (increment) and jump to PAINT
  0;JMP

// --------------
// CHANGE COLOR
// --------------
// Change color from black to white (0 to -1)
// with 2's complemet, set @i to 0 and call PAINT

(CCOLOR)
  @color
  D=M
  M=!D

  @0
  D=A
  @i
  M=D

  @PAINT
  0;JMP

// ---------------

// --------------
// PAINT
// --------------
// Loop and set SCREEN memory map to @color

(PAINT)
  @i              // while i < @monitor
  D=M
  @monitor
  D=D-M
  @INPUT          // if (i - @monitor) > 0 goto INPUT [done]
  D;JGT

  @SCREEN         // base
  D=A
  @i              // offset
  D=D+M
  @offset         // store base+offset in @offset
  M=D

  @color          // get @color( black | white )
  D=M
  @offset         // set word to @color
  A=M
  M=D

  @i              // increment counter
  M=M+1

  @PAINT
  0;JEQ
// ---------------
